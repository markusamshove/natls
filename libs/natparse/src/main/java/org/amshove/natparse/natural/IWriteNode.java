package org.amshove.natparse.natural;

public interface IWriteNode extends IStatementNode, ICanHaveReportSpecification
{
}
