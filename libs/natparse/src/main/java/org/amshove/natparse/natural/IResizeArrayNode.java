package org.amshove.natparse.natural;

public interface IResizeArrayNode extends IStatementNode
{
	IVariableReferenceNode arrayToResize();
}
