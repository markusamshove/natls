package org.amshove.natparse.natural;

public interface IPosNode extends IOperandNode
{
	IVariableReferenceNode positionOf();
}
