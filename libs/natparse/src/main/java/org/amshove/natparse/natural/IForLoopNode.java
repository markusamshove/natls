package org.amshove.natparse.natural;

public interface IForLoopNode extends IStatementWithBodyNode
{
	IOperandNode upperBound();
}
