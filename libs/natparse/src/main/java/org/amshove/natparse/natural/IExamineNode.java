package org.amshove.natparse.natural;

public interface IExamineNode extends IStatementNode
{
	IOperandNode examined();
}
