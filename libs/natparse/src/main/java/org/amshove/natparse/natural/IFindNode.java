package org.amshove.natparse.natural;

public interface IFindNode extends IStatementWithBodyNode
{
	ISymbolReferenceNode viewReference();
}
