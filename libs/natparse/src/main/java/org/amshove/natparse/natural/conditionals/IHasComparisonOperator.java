package org.amshove.natparse.natural.conditionals;

public interface IHasComparisonOperator
{
	ComparisonOperator operator();
}
