package org.amshove.natls.languageserver;

import org.amshove.natparse.natural.VariableScope;

public record UsingToAdd(String name, VariableScope scope)
{
}
