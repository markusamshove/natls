package org.amshove.natls;

public class CustomCommands
{
	public static final String CODELENS_NON_INTERACTIVE = "";
	public static final String CODELENS_SHOW_REFERENCES = "natls.codelens.showReferences";

	private CustomCommands() {}
}
