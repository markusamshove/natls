# Implemented statements

This document tracks the implementation status of Natural statements.

Legend:

:x: - not implemented

:white_check_mark: - implemented

partial - partially implemented to prevent false positives

| Statement | Status |
| --- | -- |
| ACCEPT/REJECT | :x: |
| ADD | :x: |
| ASSIGN | :x: |
| AT BREAK | :white_check_mark: |
| AT END OF DATA | :white_check_mark: |
| AT END OF PAGE | :white_check_mark: |
| AT START OF DATA | :white_check_mark: |
| AT TOP OF PAGE | :white_check_mark: |
| BACKOUT TRANSACTION | :x: |
| BEFORE BREAK PROCESSING | :white_check_mark: |
| CALL | :x: |
| CALL FILE | :x: |
| CALL LOOP | :x: |
| CALLDBPROC (SQL) | :x: |
| CALLNAT | :white_check_mark: |
| CLOSE CONVERSATION | :x: |
| CLOSE PC FILE | :x: |
| CLOSE PRINTER | :white_check_mark: |
| CLOSE WORK FILE | :x: |
| COMMIT (SQL) | :x: |
| COMPRESS | :x: |
| COMPUTE | :x: |
| CREATE OBJECT | :x: |
| DECIDE FOR | :x: |
| DECIDE ON | :x: |
| DEFINE CLASS | :x: |
| DEFINE DATA | :x: |
| DEFINE FUNCTION | :x: |
| DEFINE PRINTER | :white_check_mark: |
| DEFINE PROTOTYPE | :x: |
| DEFINE SUBROUTINE | :white_check_mark: |
| DEFINE WINDOW | partial |
| DEFINE WORK FILE | :x: |
| DELETE | :x: |
| DELETE (SQL) | :x: |
| DISPLAY | :x: |
| DIVIDE | :x: |
| DO/DOEND | :x: |
| DOWNLOAD PC FILE | :x: |
| EJECT | :white_check_mark: |
| END | :white_check_mark: |
| END TRANSACTION | :x: |
| ESCAPE | :white_check_mark: |
| EXAMINE | :white_check_mark: |
| EXPAND | :x: |
| FETCH | :white_check_mark: |
| FIND | :x: |
| FOR | :white_check_mark: |
| FORMAT | partial |
| GET | :x: |
| GET SAME | :x: |
| GET TRANSACTION DATA | :x: |
| HISTOGRAM | :partial: |
| IF | :white_check_mark: |
| IF SELECTION | :x: |
| IGNORE | :white_check_mark: |
| INCLUDE | :white_check_mark: |
| INPUT | :x: |
| INSERT (SQL) | :x: |
| INTERFACE | :x: |
| LIMIT | :x: |
| LOOP | :x: |
| METHOD | :x: |
| MOVE | :x: |
| MOVE INDEXED | :x: |
| MULTIPLY | :x: |
| NEWPAGE | :white_check_mark: |
| OBTAIN | :x: |
| ON ERROR | :x: |
| OPEN CONVERSATION | :x: |
| OPTIONS | :x: |
| PARSE XML | :x: |
| PASSW | :x: |
| PERFORM | :white_check_mark: |
| PERFORM BREAK PROCESSING | :x: |
| PRINT | :x: |
| PROCESS | :x: |
| PROCESS COMMAND | :x: |
| PROCESS PAGE | :x: |
| PROCESS SQL (SQL) | :x: |
| PROPERTY | :x: |
| READ | :x: |
| READ RESULT SET (SQL) | :x: |
| READ WORK FILE | :x: |
| READLOB | :x: |
| REDEFINE | :x: |
| REDUCE | :x: |
| REINPUT | :x: |
| REJECT | :x: |
| RELEASE | :x: |
| REPEAT | :x: |
| REQUEST DOCUMENT | :x: |
| RESET | :x: |
| RESIZE | :x: |
| RETRY | :x: |
| ROLLBACK (SQL) | :x: |
| RUN | :x: |
| SELECT (SQL) | :x: |
| SEND METHOD | :x: |
| SEPARATE | :x: |
| SET CONTROL | :x: |
| SET GLOBALS | :x: |
| SET KEY | partial |
| SET TIME | :x: |
| SET WINDOW | :x: |
| SKIP | :white_check_mark: |
| SORT | :x: |
| STACK | :white_check_mark: |
| STOP | :x: |
| STORE | :x: |
| SUBTRACT | :x: |
| SUSPEND IDENTICAL SUPPRESS | :x: |
| TERMINATE | :x: |
| UPDATE | :x: |
| UPDATE (SQL) | :x: |
| UPDATELOB | :x: |
| UPLOAD PC FILE | :x: |
| WRITE | partial |
| WRITE TITLE | :x: |
| WRITE TRAILER | :x: |
| WRITE WORK FILE | :x: |
